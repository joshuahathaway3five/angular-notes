import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import {NgmodelComponent} from './components/ngmodel/ngmodel.component';
import {SuperHeroComponent} from './components/super-hero/super-hero.component';
import {PostHttpClientComponent} from './components/post-service/post-service.component';


// this is where we're going to route our paths
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'Characters',
    component: NgmodelComponent
  },
  {
    path: 'movies',
    component: SuperHeroComponent
  },
  {
    path: 'posts',
    component: PostHttpClientComponent
  },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
