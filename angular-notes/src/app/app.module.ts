import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './components/user.component';
import { helloComponent } from './components/hello.component';
import { NameOfComponentComponent } from './components/name-of-component/name-of-component.component';
import { MathComponent } from './components/math/math.component';
import { User} from './models/User';
import { VipComponent } from './components/vip/vip.component';
import { StringsComponent } from './components/strings/strings.component';
import { BondsComponent } from './components/bonds/bonds.component';
import { SuperHeroComponent } from './components/super-hero/super-hero.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialComponent } from './components/material/material.component';

import { MatSliderModule } from '@angular/material/slider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { FormsModule } from '@angular/forms';
import {PostFormComponent} from './components/post-form/post-form.component';
import {PostService} from './services/post.service';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';



@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    helloComponent,
    NameOfComponentComponent,
    MathComponent,
    UserComponent,
    VipComponent,
    StringsComponent,
    BondsComponent,
    SuperHeroComponent,
    MaterialComponent,
    BindingComponent,
    ngmodelComponent,
    PostServiceComponent,
    PostFormComponent,
    PostHttpClientComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
    MatRadioModule,
    MatGridListModule,
    MatSelectModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
