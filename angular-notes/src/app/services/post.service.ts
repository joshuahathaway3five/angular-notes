import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PostHttpClientt} from '../models/PostHttpClientt';
import {PostHttpClientComponent} from '../components/post-service/post-service.component';


// send a header value of the content type
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PostService {
  // property
  // set a url as a property
  postURL = 'https://jsonplaceholder.typicode.com/posts';

  // inject the httpClient as a dependency
  constructor(private http: HttpClient) { }

  // create a method that will make our GET request
  getPosts(): Observable<PostHttpClientt[]> {
    // return all the data that comes with our postURL property
    return this.http.get<PostHttpClientt[]>(this.postURL);
  }


  // create a method for our POST request
  savePost(post: PostHttpClientt): Observable<PostHttpClientt> {
    return this.http.post<PostHttpClientt>(this.postURL, post, httpOptions);
  }

  updatePost(post: PostHttpClientt): Observable<PostHttpClientt> {
    const url = `${this.postURL}/${post.id}`;

    return this.http.put<PostHttpClientt>(url, post, httpOptions);

  }
//
//




  deletePost(post: PostHttpClientt | number): Observable<PostHttpClientt> {
    const id = typeof post === 'number' ? post : post.id;

    const url = `${this.postURL}/${id}`;

    return this.http.delete<PostHttpClientt>(url, )

  }
}
