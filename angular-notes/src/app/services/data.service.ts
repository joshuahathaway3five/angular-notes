import { Injectable } from '@angular/core';
import {DCHeroEvents} from '../models/DCHeroEvents';
import { Observable, of } from 'rxjs';


// Observables
// "data strea,'
// we can subscribe
// improt { of } from 'rxjs/observable/of';
// this will allow us to return an array as an observable

@Injectable({
  providedIn: 'root'
})
export class DataService {


  // initialize the characters property and assign it to an array
  characters: DCHeroEvents[];

  // property for our observable
  data: observable <any>;

  constructor()  {
  this.characters = [
    {
      persona: 'Superman',
      firstName: 'Clark',
      lastName: 'Kent',
      age:  54,
      address:  {
        street: '27 Smallville',
        city: 'Metorpolis',
        state: 'IL'
      },
      img: '',
      isActive: true,
      balance:  12300000,
      memberSince: new Date('05/01/1939 8:30:00'),
      hide: false
    },
    {
      persona:  'Raven',
      firstName:  'Rachel',
      lastName: 'Roth',
      age:  134,
      address: {
        street: '1600 Main st',
        city: 'Los Angeles',
        state: 'CA'
      },
      img: '',
      hide: false,
    },
    {
      persona:  'Batman',
      firstName:  'Bruce',
      lastName: 'Wayne',
      age:  43,
      address:{
        street: '50 Wayne Manor',
        city: 'Gotham City',
        state: 'NY'
      },
      img: '',
      isActive: true,
      balance: 999999999999999,
      hide: false
    },
  ]; // end of array
}



// create a method that will return a type of DCHeroEvents[]
// getCharacters(): DCHeroEvents[] {
//     return this.characters;
// }


  getCharacters(): Observable<DCHeroEvents[]> {
    console.log('getting dataService');
    return of(this.characters);
  }
// THIS WILL BREAK OUR CODE
  // because we're dealing with asychronous
  // now need to refactor our ngmodel.component.ts

addCharacter(character: DCHeroEvents) {
    this.characters.unshift(character);
}


// create a method, subscribe to our observable
getData() {
    this.data = new Observable(observer => {
      setTimeout(() => {
        observer.next(1);
      }, 1000);
      setTimeout(() => {
          observer.next(2);
        }, 2000);
      setTimeout(() => {
            observer.next(3);
          }, 3000);
      setTimeout(() => {
              observer.next(4);
            }, 4000);
    });
    return this.getData();
  }}
