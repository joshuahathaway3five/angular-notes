export interface Athletic {
  name: string;
  team: string;
  jerseyNo: number;
  isActive: boolean;

}
