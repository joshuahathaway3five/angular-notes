// Create a new interface named Superhero in your models directory.
// Your interface should have six properties: title, yearReleased, actors
// (at least 2, director, genre, rating (PG, or R, etc.), awards (contains at least 3 if received)

export interface SuperHero {

  title: string,
  yearReleased: number,
  actors: any,
  director: string,
  genre: string,
  rating: string,
  awards: string












}
