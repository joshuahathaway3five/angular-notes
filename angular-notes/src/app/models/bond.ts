export interface bond {

  name: string,
  yearEstablished: number,
  vocalist: string,
  guitarist: string,
  bass: string,
  drums: string,
  albums: any
}
