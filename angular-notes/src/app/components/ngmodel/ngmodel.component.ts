import {Component, OnInit, ViewChild} from '@angular/core';
import {DCHeroEvents} from '../../models/DCHeroEvents';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-ngmodel',
  templateUrl: './ngmodel.component.html',
  styleUrls: ['./ngmodel.component.css']
})
export class NgmodelComponent implements OnInit {

  // properties
  // a property for the new character entry
  // - set each property of the object to empty/ null value
  character: DCHeroEvents = {
    persona: '',
    firstName: '',
    lastName: '',
    age: null,
    address: {
      street: '',
      city: '',
      state: ''
    },
    isActive: null,
    memberSince: ''
  };

  characters: DCHeroEvents[];
  enableAddUser: boolean;
  currentClasses: {}; // an empty object
  currentStyle: {}; // empty object

  showForm: boolean;

  // being used to subscribe to our Observable...
  data: any;


  // SUBMIT LESSON
  @ViewChild('AlbumForm')form: any;
  /*
  Calling ViewChild and passed in the name of our form 'characterForm'
  * making this our "Form Identifier"
   */

  // inject the data service in the constructor
  constructor(private dataService: DataService) { }
  /*
  private = can't be used anywhere else, only within this class
  dataService = variable
  DataService = setting our variable to the DataService
  that we brought in
  Now we should be able to access any methods inside of
  our DataService
   */


  ngOnInit(): void {
    this.enableAddUser = true;
    this.showForm = false;
    // adding data of array of characters
    // this.characters = [
    //   {
    //     persona: 'Superman',
    //     firstName: 'Clark',
    //     lastName: 'Kent',
    //     age: 54,
    //     address: {
    //       street: '27 Smallville',
    //       city: 'Metropolis',
    //       state: 'IL'
    //     },
    //     img: '../assets/img/644-superman.jpg',
    //     isActive: true,
    //     balance: 12300000,
    //     memberSince: new Date('05/01/1939 8:30:00'),
    //     hide: false
    //   },
    //   {
    //     persona: 'Raven',
    //     firstName: 'Rachel',
    //     lastName: 'Roth',
    //     age: 134,
    //     address: {
    //       street: '1600 Main St',
    //       city: 'Los Angeles',
    //       state: 'CA'
    //     },
    //     img: '../assets/img/542-raven.jpg',
    //     isActive: false,
    //     memberSince: new Date('5/19/1939 8:30:00'),
    //     hide: false
    //   },
    //   {
    //     persona: 'Batman',
    //     firstName: 'Bruce',
    //     lastName: 'Wayne',
    //     age: 43,
    //     address: {
    //       street: '50 Wayne Manor',
    //       city: 'Gotham City',
    //       state: 'NY'
    //     },
    //     img: '../assets/img/70-batman.jpg',
    //     isActive: true,
    //     memberSince: new Date('01/01/1937 8:45:45'),
    //     hide: false
    //
    //   },
    // ]; // end of array

    this.setCurrentClasses();
    this.setCurrentStyle();


    // access the getCharacters() that's inside of our DataService
    // this.characters = this.dataService.getCharacters();

    // refactor due to our observable in our dataService
    // this.characters = this.dataService.getCharacters();
    this.dataService.getCharacters().subscribe(c => {
      this.characters = c;
    });

    // Subscribing to our Observable:
    // 1. how can I access getData() inside of DataService
    // 2. subscribe to our observable
    this.dataService.getData().subscribe(data => {
      console.log(data);
    });



  }

  // METHODS
  // ngClass
  setCurrentClasses() {
    this.currentClasses = {
      'btn-success': this.enableAddUser
    };
  }

  // ngStyle
  setCurrentStyle() {
    this.currentStyle = {
      'padding-top': '60px',
      'text-decoration': 'underline'
    };
  }

  toggleInfo(character) {
    // alert('Button was clicked');
    character.hide = !character.hide;
  }

  // create a method that will add 1000 to the character's balance
  //    when a button is clicked
  // lodsofemone(character){
  //   // console.log(character.balance);
  //   if (character.balance == undefined){
  //     character.balance = 3.5;
  //     alert('Bout three fiddy')
  //   }
  //   character.balance += 1000;
  // }

//  methods for the types of events
  triggerEvent(e) {
    console.log(e.type);
  }


  // method that will set showForm to true / false
  toggleForm() {
    // alert('Toggle form button clicked');
    this.showForm = !this.showForm;
  }

  fireEvent(e) {
    console.log(e.type);
    console.log(e.target.value);
  }

  onSubmit({value, valid}: {value: DCHeroEvents, valid: boolean}) {

    if (!valid) {
      console.log('Form is not valid');
    }
    else {
      value.isActive = true;
      value.memberSince = new Date();
      value.hide = false;
      console.log(value);

    }

    // this.characters.unshift(value);
    this.dataService.addCharacter(value);
    // fetching our characters from the service and adding it through the service
    // ** only working because we're working synchronous

    this.form.reset();

  }

  // passing in an object as our method's argument
  // value set as a DCHeroEvents Type
  // valid set as a boolean type


  // add new character
  // addCharacter() {
  // what we to do?
  //   this.characters.unshift(this.character);
  //
  //   // clear out the form / reset the form
  //   this.character =  {
  //     persona: '',
  //     firstName: '',
  //     lastName: '',
  //     age: null,
  //     address: {
  //       street: '',
  //       city: '',
  //       state: ''
  //     },
  //     isActive: false,
  //     memberSince: false
  //   }
  // }

}

