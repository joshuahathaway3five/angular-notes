import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-name-of-component',
  templateUrl: './name-of-component.component.html',
  styleUrls: ['./name-of-component.component.css']
})
export class NameOfComponentComponent implements OnInit {
  // PROPERTIES - an attribute of a component
  // go inside of the class
  firstname = 'charlie';
  lastnane = 'cohort';
  age = 33;




  constructor() {
  console.log("hello from users")
  this.greeting();
  }
  ngOnInit(): void {
  }

  // METHODS - function inside of a component's class
  greeting() {
    console.log("hello there, " + this.firstname);
    // in order to access properties OR methods, you need to use 'this' keyword.
  }

} // end of class
