import { Component, OnInit } from '@angular/core';
import {log} from "util";

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
 //properties
 //  num1 = 1;
 //  num2 = 2;

  //more practical

  //nameofproperty: type
  num1: number;
  num2: number;
  num4 = 5;
  num5 = 5;

  constructor() {
    // this.addition(4, 8);
    // this.sub(12, 8);
    this.fizzbuzz();
  }

  ngOnInit(): void {
  }

  // methods
  addition(a, b) {
    this.num1 = a;
    this.num2 = b;
    console.log(`${a} + ${b} = ${a + b}`);
  }

  sub(a, b) {
    this.num1 = a;
    this.num2 = b;
    console.log(`${a} - ${b} = ${a - b}`);
  }

  multiply(a, b) {
    this.num1 = a;
    this.num2 = b;
    console.log(`${a} x ${b} = ${a * b}`);
  }

  divide(a, b) {
    this.num1 = a;
    this.num2 = b;
    console.log(`${a} / ${b} = ${a / b}`);
  }

//  FIZZBUZZ
  fizzbuzz() {
    for (var i = 1; i <= 100; i++) {
      if (i % 3 === 0 && i % 5 === 0) {
        console.log('FizzBuzz');
      }
      else if (i % 3 === 0) {
        console.log('Fizz')
      }
      else if (i % 5 === 0) {
        console.log('Buzz')
      }
      else {
        console.log(i);
      }
    }
  }

}
