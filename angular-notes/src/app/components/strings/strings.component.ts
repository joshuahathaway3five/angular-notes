import { Component, OnInit } from '@angular/core';
import {Athletic} from "../../models/Athletic";

@Component({
  selector: 'app-strings',
  templateUrl: './strings.component.html',
  styleUrls: ['./strings.component.css']
})
export class StringsComponent implements OnInit {
  //properties
  Athletes: Athletic[]; // pointing to our athletic interface


  constructor() {
    this.Athletes = [{
      name: 'Michael Jordan',
      team: 'Chicago Bulls',
      jerseyNo: 23,
      isActive: false
    },
      {
        name: 'LeBron James',
        team: 'Los Angeles Lakers',
        jerseyNo: 23,
        isActive: true
      }] // end of this.athlete array

    // calling our addAthlete method inside our constructor
    // and filled in the requirments for our athletic interface

  this.addAthlete({
      name: 'Stephen Curry',
      team: 'Golden State Warriors',
        jerseyNo: 30,
      isActive: true
    });
  }

  ngOnInit(): void {

  }
  //method
  showStatus() {
      return`This player is currently playing`
  }

  //create a method named 'addAthlete' that takes in an athlete as a Athletic type
  //and adds it as a new athlete to the array
  addAthlete(Athlete: Athletic) {
    console.log('addAthlete is connected...')
    this.Athletes.push(Athlete)
  }
}

