import { Component } from "@angular/core";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent {
  // properties
  fname: string;
  lname: string;
  age: number;

  constructor() {
    this.fname = 'Charlie';
    this.lname = 'Cohort';
    this.age = 33;
  }

  // method
  greeting() {
    console.log(`Hello from ${this.fname} ${this.lname}, I am ${this.age} years old`)
  }

}

