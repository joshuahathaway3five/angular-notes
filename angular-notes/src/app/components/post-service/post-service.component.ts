import { Component, OnInit } from '@angular/core';
import {PostHttpClientt} from '../../models/PostHttpClientt';
import {PostService} from '../../services/post.service';

@Component({
  selector: 'app-post-http-client',
  templateUrl: './post-http-client.component.html',
  styleUrls: ['./post-http-client.component.css']
})
export class PostHttpClientComponent implements OnInit {
  // property
  posts: PostHttpClientt[];

  currentPost: PostHttpClientt = {
    id: 0,
    title: '',
    body: ''
  };

  // property for our update button
  isEdit = false;

  // inject our service as a dependency
  // Dependency Injection
  constructor(private postService: PostService) { }

  // fetch the posts when ngOnInit() method is initialized
  ngOnInit(): void {
    // subscribe to our Observable that's in our PostService
    this.postService.getPosts()
      .subscribe(p => {
        // console.log(p);
        this.posts = p;
      });
  }

  // create onNewPost() method
  // tslint:disable-next-line:typedef
  onNewPost(post: PostHttpClientt) {
    this.posts.unshift(post);
  }

  // create the method for onUpdatedPost()
  // tslint:disable-next-line:typedef
  onUpdatedPost(post: PostHttpClientt) {
    this.posts.forEach((current, index) => {
      if (post.id === current.id) {
        this.posts.splice(index, 1);
        this.posts.unshift(post);
        this.isEdit = false;
        this.currentPost = {
          id: 0,
          title: '',
          body: ''
        };
      }
    });
  }

  // create editPost() method
  // tslint:disable-next-line:typedef
  editPost(post: PostHttpClientt) {
    this.currentPost = post;

    // included for the update button
    this.isEdit = true;
  }

  // create the method for removePost()
  // tslint:disable-next-line:typedef
  removePost(post: PostHttpClientt) {
    if (confirm('Are you sure?')) {
      this.postService.deletePost(post.id)
        .subscribe(() => {
          this.posts.forEach((current, index) => {
            if (post.id === current.id) {
              this.posts.splice(index, 1);
            }
          });
        });
    }
  }

}
