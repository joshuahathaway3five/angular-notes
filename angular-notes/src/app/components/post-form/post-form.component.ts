import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import { PostHttpClientt } from '../../models/PostHttpClientt';
import { PostService } from '../../services/post.service';


@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  // properties
  post: PostHttpClientt;
  @Output() newPost: EventEmitter<PostHttpClientt> = new EventEmitter();

  @Output() updatedPost;

  @Input() currentPost: PostHttpClientt;

  @Input() isEdit: boolean;


  // inject our dependency
  constructor(private postService: PostService) { }

  ngOnInit(): void {
  }

  // create addPost() method
  // addPost(title, body) {
  //   console.log(title, body);
  // }

  // update our addPost() method by using the PostService
  // addPost(title, body) {
  //   if (!title || !body) {
  //     alert('Please add to entry')
  //   }
  //   else {
  //     console.log(title, body);
  //     this.postService.savePost({title, body} as PostHttpClient)
  //       .subscribe(post => {
  //         console.log(post);
  //       })
  //   }
  // }


  // update our addPost() method by attaching the event emitter
  addPost(title, body) {
    if (!title || !body) {
      alert('Please add to entry');
    }
    else {
      console.log(title, body);
      this.postService.savePost({title, body} as PostHttpClientt)
        .subscribe(post => {
          // console.log(post);

          this.newPost.emit(post);


        });
    }
  }

  updatePost() {
    console.log('blank');
    this.postService.updatePost(this.currentPost)
      .subscribe(p => {
        this.isEdit = false;
        this.updatedPost.emit(p);


      }
    )
  }
}
