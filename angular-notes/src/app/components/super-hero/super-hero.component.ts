import { Component, OnInit } from '@angular/core';
import {SuperHero} from "../../models/SuperHero";
import {bond} from "../../models/bond";

@Component({
  selector: 'app-super-hero',
  templateUrl: './super-hero.component.html',
  styleUrls: ['./super-hero.component.css']
})
export class SuperHeroComponent implements OnInit {

  superhero: SuperHero[]

  constructor() {}

  ngOnInit(): void {
    this.superhero = [{
      title: "Tron Legacy",
      yearReleased: 2010,
      actors:[" Garrett Hedlund, Jeff Bridges, Olivia Wilde"],
      director: "Joseph Kosinski ",
      genre: "Sci-Fi",
      rating: "PG",
      awards: "Saturn Award For Best Production Design"

    }]
    this.addBond({
      title: "Tron Legacy",
      yearReleased: 2010,
      actors:[" Garrett Hedlund, Jeff Bridges, Olivia Wilde"],
      director: "Joseph Kosinski ",
      genre: "Sci-Fi",
      rating: "PG",
      awards: "Saturn Award For Best Production Design"


    })
  }

  addBond(NewSuperhero: SuperHero) {
    this.superhero.push(NewSuperhero)
  }

}
