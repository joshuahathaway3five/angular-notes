import { Component, OnInit } from '@angular/core';
import { bond } from '../../models/bond';

@Component({
  selector: 'app-bonds',
  templateUrl: './bonds.component.html',
  styleUrls: ['./bonds.component.css']
})
export class BondsComponent implements OnInit {

  Bonds: bond[];

  constructor() {
    this.Bonds = [{
      name: 'LoadedDiper',
      yearEstablished: 2011,
      vocalist: 'Rodrick',
      guitarist: 'Billy',
      bass: 'Evan',
      drums: 'Dank',
      albums: ['boom baby, pacifier of death, unknown']


    }];

    this.addBond( {
      name: 'LoadedDiper2',
      yearEstablished: 2012,
      vocalist: 'Rodrick2',
      guitarist: 'Billy2',
      bass: 'Evan2',
      drums: 'Dank2',
      albums: ['boom baby2, pacifier of death2, unknown2']
    });
  }

  ngOnInit(): void {
  }

  addBond(NewBond: bond) {
    this.Bonds.push(NewBond);
  }


}
